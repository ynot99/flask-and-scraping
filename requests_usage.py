import requests

data = requests.get("http://api.open-notify.org/astros.json")
ppl = data.json()
for person in ppl["people"]:
    print(person["name"])
    print(person["craft"])
    print("-" * 20)


resource = requests.get("https://imgs.xkcd.com/comics/python_environment.png")
with open("python.png", "wb") as f:
    f.write(resource.content)

repos = requests.get("https://api.github.com/search/repositories", params={"q": "anime:javascript"})
# print(repos.json()) DO NOT RUN
