from flask import Flask, render_template, request
from flask.helpers import url_for
from werkzeug.utils import redirect

from flask_login import LoginManager, current_user, login_user
from UserModel import User
from LoginForm import LoginForm


app = Flask(__name__)
login = LoginManager(app)


@login.user_loader
def load_user(user_id):
    if user_id == 1:
        user = User(1, "Username", "Name")
        user.is_authenticated = True
        return user


@app.route("/login", methods=["GET", "POST"])
def login():
    method = request.method
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    # form = LoginForm()
    # if form.validate_on_submit():
    #     if form.username.data == 'username':
    #         user = User(1, 'username', 'Name')

    # login_user(user, remember=form.remember.)

    return "OH, HELLO THERE"


@app.route("/")
def hello_world():
    return "<h1>Hello, World!</h1><br><div><b>Bold text</b></div>"


@app.route("/help")
def help_route():
    return "<h2>There is no help, but be strong!</h2>"


@app.route("/user/<int:user_id>")
def user_route(user_id):
    return f"<h2>OH, HELLO {user_id}</h2>"


@app.route("/user/<string:username>")
def username_route(username):
    return f"<h2>OH, HELLO {username}</h2>"


@app.route("/users")
def users_route():
    return render_template("index.j2", users=[i for i in range(10)])


@app.route("/users/<name>")
def users_name_route(name):
    return render_template("index.j2", name=name, users=[i for i in range(10)])


if __name__ == "__main__":
    app.run()
